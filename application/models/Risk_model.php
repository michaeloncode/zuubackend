<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 17/01/2019
 * Time: 14:15
 */

class Risk_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->tableName = 'risks';
    }

    public function get(){
        return $this->db->get($this->tableName)->result();
    }
}