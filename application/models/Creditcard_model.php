<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 03/06/2019
 * Time: 19:19
 */

class Creditcard_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->_creditCardTable = 'credit_cards';
        $this->_cipher = 'aes-256-cbc';

    }

    private function creditCardTables(){
        return [
            'card_number', 'expiring_month', 'expiring_year', 'cvv', 'owner'
        ];
    }

    private function encrypt($data, $salt, $iv){
        return openssl_encrypt($data, $this->_cipher, $salt, OPENSSL_RAW_DATA , $iv);
    }

    private function decrypt($data, $salt, $iv){
        return openssl_decrypt($data, $this->_cipher, $salt, OPENSSL_RAW_DATA, $iv);
    }

    public function getByUserIDAndDecrypt($userID){
        $data = $this->getByUserID($userID);
        if(!empty($data)){
            $creditCardTables = $this->creditCardTables();
            if(!empty($creditCardTables)){
                foreach ($creditCardTables as $table){
                    $data[$table]=$this->decrypt($data[$table], $data['salt'], $data['iv']);
                }
            }
            unset($data['salt'], $data['iv'], $data['active']);
        }
        return $data;
    }

    public function deleteForUser($userID){
        $this->db->update($this->_creditCardTable,['active'=>0], ['user_id'=>$userID]);
    }

    public function insertOrUpdate($data){
        $ivlen = openssl_cipher_iv_length($this->_cipher);
        $data['iv'] = openssl_random_pseudo_bytes($ivlen);
        $data['salt']=$this->ion_auth->salt();
        $data['active']=1;
        $creditCardTables = $this->creditCardTables();
        if(!empty($creditCardTables)){
            foreach ($creditCardTables as $table){
                $data[$table]=$this->encrypt($data[$table], $data['salt'], $data['iv']);
            }
        }
        if($this->userHasCreditCard($data['user_id'])){
            $this->deleteForUser($data['user_id']);
        }
        $data['created_at']=time();
        $this->db->insert($this->_creditCardTable, $data);
    }

    public function userHasCreditCard($userID){
        return (bool) $this->db->query("SELECT count(id) as number from $this->_creditCardTable where user_id=$userID and active=1")->row()->number;
    }

    public function getByUserID($userID, $allFields=true){
        $fields= $allFields===true ? '*' : $allFields;
        $this->db->select($fields);
        return $this->db->get_where($this->_creditCardTable, ['user_id'=>$userID, 'active'=>1])->row_array();

    }
}