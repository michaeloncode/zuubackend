<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 25/05/2019
 * Time: 13:54
 */

class Payment_model extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->_paymentMeansTable = 'payment_means';
    }

    public function getAll(){
        return $this->db->get($this->_paymentMeansTable)->result_array();
    }
}