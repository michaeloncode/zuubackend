<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 06/06/2019
 * Time: 10:11
 */

class Problemreport_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->problemReportTable= 'problem_report';
    }

    public function insert($data){
        $data['created_at']=date('Y-m-d G:i:s');
        $this->db->insert($this->problemReportTable, $data);
    }
}