<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 26/10/2018
 * Time: 23:30
 */

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    private function get_metas_group()
    {
        return array(
            'confirm_code',
            'password_modification_code',
            'country',
            'isProfilModified',
            'isPasswordModified',
            'userpic',
            'sex',
            'city',
            'userAccountIsValid',
            'id_card_pic',
            'birthday',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
            'origin_city',
            'school_info',
            'profession',
            'website',
            'language',
            'religion',
            'political_opinion',
            'about',
            'idCardControl',
            'selfiePicControl',
            //'selfieUrl'
            //generatedSelfieCode
        );
    }

    private function getNotesMetasGroup(){
        return [
            'identity_note',
            'paymentMean_note',
            'profile_note',
            'firstName_note',
            'lastName_note',
            'username_note',
            'sex_note',
            'phone_note',
            'email_note',
            'birthday_note',
            'company_note',
            'profession_note',
            'facebook_note',
            'twitter_note',
            'linkedin_note',
            'language_note',
            'religion_note',
            'about_note',
            'rating_note',
        ];
    }

    public function getNotesMeta($userID){
        $metas = $this->getNotesMetasGroup();
        $userArrayData=[];
        if (!empty($metas)) {
            foreach ($metas as $meta) {
                $userArrayData[$meta] = (int) $this->get_meta($userID, $meta);
            }
        }
        return $userArrayData;
    }

    public function getUserMeta(array $userArrayData, $additionalMetas=[], $useOnlyAdditionalMetas=false, $idField='id')
    {
        if (!empty($userArrayData)) {
            if(!$useOnlyAdditionalMetas){
                $metas = $this->get_metas_group();
                if (!empty($metas)) {
                    foreach ($metas as $meta) {
                        $userArrayData[$meta] = $this->get_meta($userArrayData[$idField], $meta);
                    }
                }
            }
            if(!empty($additionalMetas)){
                foreach ($additionalMetas as $additionalMeta){
                    $userArrayData[$additionalMeta] = $this->get_meta($userArrayData[$idField], $additionalMeta);
                }
            }
        }
        return $userArrayData;
    }


    public function get_meta($user_id, $key)
    {
        return get_meta($user_id, $key, 'user_meta', 'user_id');
    }

    public function megaMetaUpdate($userID, array $metasKeyValue){
        if(!empty($metasKeyValue)){
            foreach ($metasKeyValue as $key=>$meta){
                $this->update_meta($userID, $key, $meta);
            }
        }
    }

    public function update_meta($user_id, $key, $value)
    {
        update_meta($user_id, $key, $value, 'user_meta', 'user_id');
    }

    public function update($userID, $data)
    {
        $metaGroups = $this->get_metas_group();
        $metaData = [];
        if (!empty($metaGroups)) {
            foreach ($metaGroups as $group) {
                if (isset($data[$group])) {
                    $metaData[$group] = $data[$group];
                    unset($data[$group]);
                }
            }
        }
        if(!empty($data)){
            $this->ion_auth->update($userID, $data);
        }
        if (!empty($metaData)) {
            foreach ($metaData as $key => $datum) {
                $this->update_meta($userID, $key, $datum);
            }
        }
    }


    public function getGroupByName($groupName)
    {
        return $this->db->get_where('groups', ['name' => $groupName])->row();

    }

    public function getActivationCode($userID)
    {
        return $this->db->query("SELECT activation_code as code from users where id=$userID")->row()->code;
    }

    public function insert($data, $groupNames = 'members')
    {
        $username = isset($data['username']) ? $data['username'] : $data['first_name'] . $data['last_name'] . uniqid();
        $email = isset($data['email']) ? $data['email'] : uniqid() . '@getzuu.com';
        $password = isset($data['password']) ? $data['password'] : uniqid();
        $metaGroups = $this->get_metas_group();
        $data['confirm_code'] = getRandomCode();
        $metas = [];
        if (!empty($metaGroups)) {
            foreach ($metaGroups as $metaGroup) {
                if (isset($data[$metaGroup])) {
                    $metas[$metaGroup] = $data[$metaGroup];
                    unset($data[$metaGroup]);
                }
            }
        }
        unset($data['username'], $data['email'], $data['password']);
        $groupArray = [];
        if (is_array($groupNames) && !empty($groupNames)) {
            foreach ($groupNames as $groupName) {
                $groupArray[] = (int)$this->getGroupByName($groupName)->id;
            }
        } else {
            $groupArray = [(int)$this->getGroupByName($groupNames)->id];
        }

        $userData = $this->ion_auth->register($username, $password, $email, $data, $groupArray);
        if ($userData && !empty($metas)) {
            foreach ($metas as $key => $meta) {
                $this->update_meta((int)$userData['id'], $key, $meta);
            }
        }
        return $userData;

    }

    public function getGroups($except = 'members')
    {
        return $this->db->query("SELECT * from groups where name<>'$except'")->result_array();
    }


    public function mailExist($email)
    {
        return (bool)$this->db->query("SELECT COUNT(id) as nbr from users where email = '$email'")->row()->nbr;
    }

    public function userNameExist($userName)
    {
        return (bool)$this->db->query("SELECT COUNT(id) as nbr from users where username = '$userName'")->row()->nbr;
    }
    public function phoneExist($phone)
    {
        return (bool)$this->db->query("SELECT COUNT(id) as nbr from users where phone = $phone")->row()->nbr;
    }



    public function getIDByMail($email)
    {
        $result = $this->db->query("SELECT id from users where email = '$email'")->row();
        return maybe_null_or_empty($result, 'id');
    }

    public function getUsersByGroup($groupNames, $onlyActiveUsers = false, $exceptUserID = '')
    {
        $sqlCond = "";
        $activeUserCond = "";
        if (is_array($groupNames) && !empty($groupNames)) {
            foreach ($groupNames as $key => $groupName) {
                $sqlCond .= ($key != 0 ? ' or ' : '') . "name='$groupName'";
            }
        } else {
            $sqlCond = "name='$groupNames'";
        }
        if ($onlyActiveUsers) {
            $activeUserCond = " (users.active=1 or users.active=2) and ";
        }
        if ($exceptUserID && $exceptUserID != '') {
            $activeUserCond .= " users.id <> $exceptUserID and";
        }

        $users = $this->db->query("SELECT * FROM users where$activeUserCond users.id IN (SELECT user_id from 
users_groups where group_id in (SELECT id from groups where $sqlCond))")->result_array();
        if (!empty($users)) {
            foreach ($users as $key => $user) {
                $users[$key]['created_on'] = getDateByTime($user['created_on']);
                $users[$key]['last_login'] = getDateByTime($user['last_login']);
                $users[$key] = $this->getUserMeta($users[$key]);
                $statusArray = $this->ion_auth->get_users_groups($user['id'])->result();
                $temp = [];
                if (!empty($statusArray)) {
                    foreach ($statusArray as $status) {
                        $temp[] = $status->description;
                    }
                }
                $users[$key]['roles'] = implode(', ', $temp);
                /*if(user_can('admin', $user['id'])){
                    $users[$key]['status'] = $this->getGroupByName('admin')->description;
                }elseif (user_can('moderator', $user['id'])){
                    $users[$key]['status'] = $this->getGroupByName('moderator')->description;
                }else{
                    $users[$key]['status'] = $this->getGroupByName('members')->description;
                }*/
            }
        }
        return $users;
    }

    public function getActive($userID)
    {
        $this->db->select('active');
        return $this->db->get_where('users', ['id' => $userID])->row()->active;
    }

    public function getCurrentUser($userID = null, $additionalMetasToGet=[])
    {
        $user = $this->ion_auth->user($userID)->row_array();
        if (!empty($user)) {
            $user = $this->getUserMeta($user, $additionalMetasToGet);
            // Obtaining user groups
            $user['groups'] = $this->ion_auth->get_users_groups($userID)->result();
        }
        return $user;
    }

    public function validate($username, $hashedOrNotPassword, /*Because of status 1 and 0*/$mustBeConfirmed = true, $isHashedPassword = true)
    {
        if((!$isHashedPassword) && $this->ion_auth->login($username, $hashedOrNotPassword, false)){
            $user=$this->getCurrentUser();
        }else{
            $user = $this->db->get_where('users', ['username' => $username, 'password' => $hashedOrNotPassword])->row();
        }
        if (!empty($user)) {
            switch (maybe_null_or_empty($user, 'active')) {
                case 0:
                    if(!$mustBeConfirmed){
                        return [
                            'status'=>true,
                            'data'=>$user
                        ];
                    }else{
                        return [
                            'status' => false,
                            'message' => 'Utilisateur non confirmé'
                        ];
                    }
                    break;
                case 1:
                    return [
                        'status'=>true,
                        'data'=>$user
                    ];
                case 2:
                    return [
                        'status' => false,
                        'message' => 'Utilisateur banni'
                    ];
            }
        }
        return [
            'status' => false,
            'message' => 'Données utilisateurs incorrects'
        ];
    }

    public function checkPhoneNumber($phone){
        $result = $this->db->query("SELECT id from users where phone=$phone")->row();
        if(!empty($result)){
            return $result->id;
        }
        return false;
    }

    public function userExistByField($identity, $field='email', $isActive=true, $returnOnlyTrueOrFalse=false){
        $conditionArray=[
            $field=>$identity
        ];
        if($isActive){
            $conditionArray['active']=1;
        }
        if($returnOnlyTrueOrFalse){
            $this->db->select('COUNT(id) as nbr');
            return (bool) $this->db->get_where('users', $conditionArray)->row()->nbr;
        }
        $user = $this->db->get_where('users', $conditionArray)->row_array();
        if(!empty($user)){

            $user=$this->getUserMeta($user);
            return $user;
        }
        return false;
    }

    public function confirm_user($userID, $confirmCode, $activationCode){
        $result=$this->db->select('active')
            ->get_where('users', ['id'=>$userID])->row();
        /*if((int)maybe_null_or_empty($result, 'active')===0){

        }else{
            return [
                'status'=>false,
                'message'=>'Utilisateur déjà confirmé'
            ];
        }*/
        $verification = $confirmCode===$this->get_meta($userID, 'confirm_code');
        if((int)maybe_null_or_empty($result, 'active')===0){
            $verification = $verification && $this->ion_auth->activate($userID, $activationCode);
        }
        if($verification){
            //$this->ion_auth->update($userID, ['active'=>1]);
            $this->update_meta($userID, 'confirm_code', null);
            return [
                'status'=>true,
                'message'=>'Confirmation avec succès',
                'data'=>$this->getCurrentUser($userID)
            ];
        }else{
            return [
                'status'=>false,
                'message'=>'Code de confirmation incorrect',
            ];
        }

    }

    public function delete_meta($userID, $key){
        $this->db->delete('user_meta', ['user_id'=>$userID, 'key'=>$key]);
    }

    public function megaMetaDelete($userID, array $keys){
        if(!empty($keys)){
            foreach ($keys as $key){
                $this->delete_meta($userID, $key);
            }
        }
    }

}