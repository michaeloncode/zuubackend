<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 07/03/2019
 * Time: 15:51
 */

class Country_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function get(){
        return $this->db->get('countries')->result_array();
    }

    public function forSelect2(){
        $this->db->select('id, code_iso3, name');
        $countries = $this->get();
        if(!empty($countries)){
            $temp=[];
            foreach ($countries as $country){
                $temp[]=[
                    'id'=>$country['id'],
                    'iso3'=>$country['code_iso3'],
                    'name'=>$country['name']
                    ] ;
            }
            return $temp;
        }
        return $countries;
    }

    public function getCitiesByCountry($codeIso3){
        return $this->db->query("SELECT distinct id, name from cities where state_id IN (SELECT states.id from states where country_id IN (SELECT countries.id from countries where code_iso3 = '$codeIso3')) order by name asc")->result();
    }
}