<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 05/06/2019
 * Time: 17:32
 */

class Contactform_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->_contactFormTable = 'contact_form';
    }

    public function insert($data){
        $data['created_at']=date('Y-m-d G:i:s');
        $this->db->insert($this->_contactFormTable, $data);
    }
}