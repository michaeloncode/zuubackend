<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 23/04/2019
 * Time: 06:55
 */

class Transport_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->_transportMeansForUserTable = 'transport_means_for_user';
        $this->_transportMeansForUserMetaTable = 'transport_means_for_user_meta';
        $this->_transportMeansTable = 'transport_means';
        $this->_transportOfferProposal = 'transport_offer_proposal';
        $this->_usersTable = 'users';
    }

    private function getTransportMeanForUserMetasGroup(){
        return array(
            'transport_pic',
            'assurance_pic',
            'driver_licence_pic',
            'brand',
            'model',
            'matriculation',
            'color',
        );
    }

    public function getTransportMeans(){
        return $this->db->get($this->_transportMeansTable)->result_array();
    }

    public function insertUserTransportMean($data){
        $metas = [];
        $metaGroups = $this->getTransportMeanForUserMetasGroup();
        if (!empty($metaGroups)) {
            foreach ($metaGroups as $metaGroup) {
                if (isset($data[$metaGroup])) {
                    $metas[$metaGroup] = $data[$metaGroup];
                }
                unset($data[$metaGroup]);
            }
        }
        $this->db->insert($this->_transportMeansForUserTable, $data);
        $transportMeanID = $this->db->insert_id();
        if ($transportMeanID) {
            foreach ($metas as $key => $meta) {
                $this->update_meta((int)$transportMeanID, $key, $meta);
            }
        }
        return $transportMeanID;
    }

    public function getUserTransportMeansByUserID($userID){
        $transportMeans = $this->db->get_where($this->_transportMeansForUserTable, ['user_id'=>$userID])->result_array();
        if(!empty($transportMeans)){
            foreach ($transportMeans as $key=>$transportMean){
                $transportMeans[$key]=$this->getTransportMeansMeta($transportMean);
            }
        }
        return $transportMeans;
    }

    public function getTransportMeansMeta(array $transportArrayData, $idField='id')
    {
        if (!empty($transportArrayData)) {
            $metas = $this->getTransportMeanForUserMetasGroup();
            if (!empty($metas)) {
                foreach ($metas as $meta) {
                    $transportArrayData[$meta] = $this->get_meta($transportArrayData[$idField], $meta);
                }
            }
        }
        return $transportArrayData;
    }

    public function isTransportMeanForUser($userID, $userTransportMeanID){
        $this->db->select('id');
        $result = $this->db->get_where($this->_transportMeansForUserTable, ['user_id'=>$userID, 'id'=>$userTransportMeanID])->row();
        return (bool) maybe_null_or_empty($result, 'id', true);
    }

    public function insertOfferTransportationProposal($data){
        $data['created_at']=date('Y-m-d G:i:s');
        $data['active']=1;
        $this->db->insert($this->_transportOfferProposal, $data);
        return $this->db->insert_id();
    }

    public function updateOfferTransportationProposal($offerID, $data){
        $data['updated_at']=date('Y-m-d G:i:s');
        $this->db->update($this->_transportOfferProposal, $data, ['id'=>$offerID]);
    }

    public function isOfferTransportationProposalForUser($userID, $offerID){
       return (bool) $this->db->query("SELECT count(id) as nbr from $this->_transportOfferProposal where 
transport_mean_for_user_id IN (SELECT id from transport_means_for_user where 
user_id = $userID) and id = $offerID")->row()->nbr;
    }

    public function getOfferTransportationProposalByUserID($userID, $forceActive=true){
        $additionalCond=($forceActive ? " and $this->_transportOfferProposal.active = 1" : '');
       return $this->db->query("SELECT $this->_transportOfferProposal.* from $this->_transportOfferProposal join $this->_transportMeansForUserTable 
ON $this->_transportOfferProposal.transport_mean_for_user_id = $this->_transportMeansForUserTable.id where $this->_transportMeansForUserTable.user_id = $userID".$additionalCond)->result();
    }

    public function getActiveOfferTransportationProposals($beforeNMinutes=5){
        $actualTimePlusRange=time()-($beforeNMinutes * 60);
        $neededUserMetas=[
            'userpic'
        ];
        $results = $this->db->query("SELECT $this->_transportOfferProposal.*, $this->_transportMeansForUserTable.*, $this->_usersTable.username, $this->_usersTable.last_name, $this->_usersTable.first_name, $this->_transportMeansTable.* 
from $this->_transportOfferProposal join $this->_transportMeansForUserTable on 
$this->_transportMeansForUserTable.id = $this->_transportOfferProposal.transport_mean_for_user_id join $this->_usersTable 
on $this->_usersTable.id = $this->_transportMeansForUserTable.user_id join $this->_transportMeansTable 
on $this->_transportMeansTable.id = $this->_transportMeansForUserTable.transport_mean_id 
where $this->_transportOfferProposal.active = 1 and $this->_transportOfferProposal.starting_date > $actualTimePlusRange")->result_array();
        if(!empty($results)){
            $this->load->model('user_model');
            foreach ($results as $key=>$result){
                $results[$key]=$this->getTransportMeansMeta($results[$key], 'transport_mean_for_user_id');
                $results[$key]=$this->user_model->getUserMeta($results[$key], $neededUserMetas, true, 'user_id');
            }
        }
        return $results;

    }

    public function getOfferTransportProposal($offerID){
        return $this->db->get_where($this->_transportOfferProposal, ['id'=>$offerID])->row_array();
    }

    public function getUserTransportMeanByTransportMeanID($transportMeanID){
        return $this->db->get_where($this->_transportMeansForUserTable, ['id'=>$transportMeanID])->row_array();
    }

    public function get_meta($user_id, $key)
    {
        return get_meta($user_id, $key, $this->_transportMeansForUserMetaTable, 'transport_means_for_user_id');
    }

    public function update_meta($user_id, $key, $value)
    {
        update_meta($user_id, $key, $value, $this->_transportMeansForUserMetaTable, 'transport_means_for_user_id');
    }
}