<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 27/10/2018
 * Time: 19:19
 */

require(APPPATH.'/libraries/REST_Controller.php');
class MY_Controller extends \Restserver\Libraries\REST_Controller {

    public function __construct()
    {
        parent::__construct();
        /*header("Access-Control-Allow-Methods: POST, GET");
        //header("Access-Control-Allow-Methods: GET, OPTIONS");
        header("Access-Control-Allow-Headers: Content-Type, Content-Length, Accept-Encoding");
        header('Access-Control-Allow-Origin: *');*/
    }
}