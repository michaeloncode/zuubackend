<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 10/01/2019
 * Time: 09:12
 */

function senditMail(){
    $ci=&get_instance();
    $ci->load->library('email');

    $subject = 'This is a test';
    $message = '<p>This message has been sent for testing purposes.</p>';

// Get full html:
    $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=' . strtolower(config_item('charset')) . '" />
    <title>' . html_escape($subject) . '</title>
    <style type="text/css">
        body {
            font-family: Arial, Verdana, Helvetica, sans-serif;
            font-size: 16px;
        }
    </style>
</head>
<body>
' . $message . '
</body>
</html>';
// Also, for getting full html you may use the following internal method:
//$body = $ci->email->full_html($subject, $message);

    $result = $ci->email
        ->reply_to('no-reply@abakescrafts.com')    // Optional, an account where a human being reads.
        ->to('animashaun1vie@gmail.com')
        ->subject($subject)
        ->message($body)
        ->send();
}