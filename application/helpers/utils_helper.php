<?php
/**
 * Created by PhpStorm.
 * User: Mike
 * Date: 29/01/2018
 * Time: 13:54
 */
defined('BASEPATH') OR exit('No direct script access allowed');
function getFormSubmit($label, $class = "btn btn--md btn--round btn-ladaa")
{
    ?>
    <button type="submit" data-style="expand-left" class="<?php echo $class ?>">
        <?php echo $label ?>
    </button>
    <?php
}

function getSlugifyString($string)
{
    return strtolower(preg_replace('/[^a-zA-Z0-9-_\.]/', '', $string));
}

function getPaymentMonths()
{
    return [
        '' => lang('month'),
        1 => lang('january'),
        2 => lang('february'),
        3 => lang('march'),
        4 => lang('april'),
        5 => lang('may'),
        6 => lang('june'),
        7 => lang('july'),
        8 => lang('august'),
        9 => lang('september'),
        10 => lang('october'),
        11 => lang('november'),
        12 => lang('december'),
    ];
}

function getPaymentYears()
{
    $actualYear = (int)date('Y');
    $years = ['' => lang('year')];
    for ($i = 0; $i <= 10; $i++) {
        $years[$actualYear + $i] = $actualYear + $i;
    }
    return $years;
}

function getCurrentURL()
{
    $currentURL = (@$_SERVER["HTTPS"] == "on") ? "https://" : "http://";
    $currentURL .= $_SERVER["SERVER_NAME"];

    if ($_SERVER["SERVER_PORT"] != "80" && $_SERVER["SERVER_PORT"] != "443") {
        $currentURL .= ":" . $_SERVER["SERVER_PORT"];
    }

    $currentURL .= $_SERVER["REQUEST_URI"];
    return $currentURL;
}

function load_css_files()
{
    $assetsUrl = base_url('assets/');
    return array(
        $assetsUrl . 'js/toast-master/css/jquery.toast.css',
        $assetsUrl . 'css/bootstrap.min.css',
        $assetsUrl . 'css/icons.css',
        $assetsUrl . 'css/metismenu.min.css',
        $assetsUrl . 'css/style.css',
        $assetsUrl . 'css/stylesheet.css',);
}

function get_form_error($name)
{
    return form_error($name, '<p class="form-error">', '</p>');
}

function load_js_files()
{
    $assetsUrl = base_url('assets/');
    return [
        $assetsUrl . 'js/modernizr.min.js',
        $assetsUrl . 'js/jquery.min.js',
        $assetsUrl . 'js/popper.min.js',
        $assetsUrl . 'js/bootstrap.min.js',
        $assetsUrl . 'js/metisMenu.min.js',
        $assetsUrl . 'js/waves.js',
        $assetsUrl . 'js/jquery.slimscroll.js',
        $assetsUrl . 'js/toast-master/js/jquery.toast.js',
        $assetsUrl . 'js/monjs.js',
        $assetsUrl . 'js/jquery.core.js',
        $assetsUrl . 'js/jquery.app.js',

    ];
}

function upload_data($args, $names, $resize = false)
{
    $args['encrypt_name'] = true;
//    var_dump($args);exit;
    $ci =& get_instance();
    $ci->load->library('upload', $args);
    $data = array();
    if (!empty($names)) {
        foreach ($names as $name) {
            if ($ci->upload->do_upload($name)) {
                $data[$name] = $ci->upload->data();
                if ($resize) {
                    $config2 = [];
                    $config2['image_library'] = 'gd2';
                    $config2['source_image'] = $data[$name]['full_path'];
//                $config2['new_image'] = './image_uploads/thumbs';
                    $config2['maintain_ratio'] = false;
                    $config2['width'] = 512;
                    $config2['height'] = 512;
                    $ci->load->library('image_lib', $config2);
                    $ci->image_lib->resize();
                }
            }
        }
        return $data;
    }
}


function get_upload_path($additional = "uploads/")
{
    return base_url($additional);
}

function getFieldInfo($info)
{
    ?>
    <small id="emailHelp" class="form-text text-muted"><?php echo $info ?></small>
    <?php
}

function sendMyResponse($param, $null=null, $continue=false){
    $ci=&get_instance();
    $ci->response($param, $null, $continue);
}

function get_warning_message($msg = "")
{
    $msg = $msg == "" ? lang('unathorized-msg-action') : $msg;
    set_flashdata($msg, 'warning');
}

function insertOrUpdateInTable($table, $data, $userID = '')
{
    $ci =& get_instance();
    if ($userID == '') {
        $ci->db->insert($table, $data);
        $userID = $ci->db->insert_id();
    } else {
        $ci->db->update($table, $data, ['id' => $userID]);
    }
    return $userID;
}

function get_error_message($msg = "")
{
    $msg = ($msg != '') ? $msg : lang('fill-out-criterias');
    set_flashdata($msg, 'error');
}

function megaUnset($data, $keys)
{
    foreach ($keys as $key) {
        unset($data[$key]);
    }
}

function uploadBase64($base64)
{
    $file = getBase64UploadFileNameAndFileData($base64);
    if (!empty($file)) {
        uploadFileDataWithFilePutContent(maybe_null_or_empty($file, 'file'), maybe_null_or_empty($file, 'data'));
        return maybe_null_or_empty($file, 'file_name');
    }
    return false;

}

function megaUploadAndOrExit(array $files, $exit = true)
{
    if (!empty($files)) {
        foreach ($files as $file) {
            uploadFileDataWithFilePutContent(maybe_null_or_empty($file, 'file'), maybe_null_or_empty($file, 'data'));
        }
    }
    if ($exit) {
        exit;
    }
}

function uploadFileDataWithFilePutContent($file, $fileData)
{
    file_put_contents($file, $fileData);
}

function getKnownImageExtensions(){
    return ['png', 'jpg', 'jpeg'];
}

function getBase64UploadFileNameAndFileData($base64, $supportedFormat = [])
{
    require_once(APPPATH . '/libraries/MimeType.php');

    /*$image_parts = explode(";base64,", $base64);

    $image_type = $image_type_aux[1];
    $image_type = getExtensionByMime($image_type);*/
    $image_base64 = base64_decode($base64/*$image_parts[1]*/);
    $f = finfo_open();
    $mime_type = finfo_buffer($f, $image_base64, FILEINFO_MIME_TYPE);
    $ci = &get_instance();
    if (MimeType::isSupported($mime_type)) {
        $fileExtension = MimeType::getExtensions($mime_type)[0];
        if (!empty($supportedFormat)) {
            if (!in_array($fileExtension, $supportedFormat)) {
                $ci->response([
                    'status' => false,
                    'message' => 'Les fichiers à uploader doivent être du format '.implode(' | ', $supportedFormat)
                ]);
            }
        }
        $ci->load->helper('path');
        $path = set_realpath('uploads/');
        $fileName = md5(uniqid()) . '.' . $fileExtension;
        $file = $path . $fileName;
        return [
            'file_name' => $fileName,
            'data' => $image_base64,
            'file' => $file
        ];
        //file_put_contents($file, $image_base64);

    }else{
        $ci->response([
            'status' => false,
            'message' => 'Format du fichier uploadé pas supporté'
        ]);
    }
    return false;
}

function getExtensionByMime($mime)
{
    $extensions = [
        'jpeg' => 'jpg',
        'png' => 'png',
    ];
    return $extensions[$mime];
}


function get_success_message($msg)
{
    set_flashdata($msg, 'success');
}

function convert_date_to_format($date, $initialFormat = 'Y-m-d G:i:s', $finalFormat = 'd/m/Y G:i:s')
{
    if ($date && is_string($date)) {
        return DateTime::createFromFormat($initialFormat, $date)->format($finalFormat);
    }
    return false;
}

function redirect_if_id_is_not_valid($id, $table_name = '', $redirect)
{
    if (is_numeric($id) && (int)$id > 0) {
        if ($table_name == '') {
            return true;
        } else {
            $CI = &get_instance();
            $query = $CI->db->query("SELECT COUNT(id) AS nombre FROM $table_name WHERE id=$id");
            if ($query->row()->nombre)
                return true;
        }
    }
    get_warning_message(lang('unauthorized-action'));
    redirect($redirect);
}

function redirectIfSlugIsNotValid($slug, $tableName, $fieldName, $redirect)
{
    if (is_string($slug)) {
        $CI = &get_instance();
        $query = $CI->db->query("SELECT id FROM $tableName WHERE $fieldName='$slug'")->row();
        if ($id = maybe_null_or_empty($query, 'id')) {
            return true;
        }
    }
    get_warning_message(lang('unauthorized-action'));
    redirect($redirect);
}

function redirectIfCurrentUserIsModeratorAndTargetIsAdmin($currentUserID, $targetUserID, $redirect)
{
    if (!user_can('admin', $currentUserID)) {
        if (user_can('admin', $targetUserID)) {
            get_warning_message(lang('unauthorized-action'));
            redirect($redirect);
        }
    }
}

function countTotalRow($tableName, $additionalCond = "")
{
    $ci =& get_instance();
    return $ci->db->query("SELECT count(id) as nbr from $tableName$additionalCond")->row()->nbr;
}

function getPagination($offset, $limit, $totalRow, $url, $get)
{
    $pages = (int)($totalRow / $limit);
    if ($pages == 0) {
        return;
    }
    if ($totalRow > ($limit * $pages)) {
        $pages = $pages + 1;
    }
    $hasNext = ($offset * $limit) < $totalRow;
    $hasPrevious = $offset > 1;
    //var_dump($url);exit;
    //$query = http_build_query($get);
    ?>
    <div class="pagination-area pagination-area2">
        <nav class="navigation pagination" role="navigation">
            <div class="nav-links">
                <?php
                if ($hasPrevious) {
                    $get['p'] = $offset - 1;
                    $query = http_build_query($get);
                    ?>
                    <a class="prev page-numbers" href="<?= "$url?$query" ?>">
                        <span class="lnr lnr-arrow-left"></span>
                    </a>
                    <?php
                }
                ?>
                <?php
                for ($i = 1; $i <= $pages; $i++) {
                    $get['p'] = $i;
                    $query = http_build_query($get);
                    ?>
                    <a class="page-numbers <?= $offset == $i ? 'current' : '' ?>"
                       href="<?= $offset == $i ? '#' : "$url?$query" ?>"><?= $i ?></a>
                    <?php
                }
                ?>
                <?php
                if ($hasNext) {
                    $get['p'] = $offset + 1;
                    $query = http_build_query($get);
                    ?>
                    <a class="next page-numbers" href="<?= "$url?$query" ?>">
                        <span class="lnr lnr-arrow-right"></span>
                    </a>
                    <?php
                }
                ?>

            </div>
        </nav>
    </div>
    <?php
}

function update_meta($id, $key, $value, $table_meta, $table_id_val)
{
    $ci =& get_instance();
    if (!empty($id) && !empty($key) && $value !== '') {
        $query = $ci->db->get_where($table_meta, array(
            $table_id_val => $id,
            'key' => $key,
        ));
        if (empty($query->row())) {
            $ci->db->insert($table_meta, array(
                $table_id_val => $id,
                'key' => $key,
                'value' => $value
            ));
        } else {
            $ci->db->where(array(
                $table_id_val => $id,
                'key' => $key,
            ));
            $ci->db->update($table_meta, array('value' => maybe_serialize($value)));
        }
    }
}

function get_form_upload($data, $extensions = '*', $format = 'portrait square landscape', $maxSize = '1M', $multiple = false)
{
    ?>
    <?php
    $args = array(
        'data-default-file' => maybe_null_or_empty($data, 'value'),
        'class' => "dropify",
        'data-max-file-size' => $maxSize,
        'data-allowed-formats' => $format,
        'data-allowed-file-extensions' => $extensions,
        'value' => set_value($data['name'], maybe_null_or_empty($data, 'value'))
    );
    if (!isset($data['value'])) {
        $args['required'] = '';
    }
    if ($multiple) {
        $args['multiple'] = '';
    }
    echo form_upload($data['name'], '', $args);
}

function getFormUploadWithCrop($data, $containerClass = 'myCropper')
{
    ?>
    <div class="<?= $containerClass ?>">
        <img class="cropping-img" src="<?= $data['default'] ?>">
        <div style="display: none" class="searc-wrap">
            <button title="<?= lang('click-to-crop') ?>" type="button" class="btn search-wrap__btn croppy">
                <span class="lnr lnr-crop"></span>
            </button>
        </div>
    </div>
    <button type="button" class="btn btn--default btn--md uploaderButton"
            data-target="#inputImage"><?= lang('choose-image') ?></button>
    <input type="hidden" <?= isset($data['required']) ? 'required=""' : '' ?> class="base64CroppedUrl"
           name="<?= $data['name'] ?>" value="">
    <?php
    get_form_error($data['name']);
    ?>
    <input style="display: none" type="file" id="inputImage" class="form-control">
    <?php
}

function maybe_null_or_empty($element, $property, $canReturnNull = false)
{
    if (is_object($element)) {
        $element = (array)$element;
    }
    if (isset($element[$property])) {
        return $element[$property];
    } else {
        return ($canReturnNull ? null : "");
    }
}

function setFormValidationRules($data)
{

    if (!empty($data)) {
        $CI =& get_instance();
        foreach ($data as $datum) {
            $CI->form_validation->set_rules($datum['name'], $datum['label'], $datum['rules'], isset($datum['custom_message']) ? $datum['custom_message'] : []);
        }
    }
}

function setErrorDelimiter()
{
    $CI = &get_instance();
    $CI->form_validation->set_error_delimiters(null, null);
    return $CI->form_validation->error_string();
}

function getRandomCode($min = 100000, $max = 999999)
{
    return random_int($min, $max);
}

function get_flashdata()
{
    $CI = &get_instance();
    if (!empty($CI->session->flashdata('message'))) {
        ?>
        <div class="flashdata" data-heading="<?= lang('notification') ?>" onload="click()"
             data-text="<?php echo $CI->session->flashdata('message') ?>" data-position="top-right"
             data-icon="<?php echo $CI->session->flashdata('alert') ?>"
        >
        </div>
        <?php
        $CI->session->set_flashdata('message', null);
    }
}

function set_flashdata($message, $alert = 'error')
{
    $CI = &get_instance();
    $CI->session->set_flashdata('message', $message);
    $CI->session->set_flashdata('alert', $alert);
}

function getImageUrl($additional = "")
{
    return base_url("assets/images/$additional");
}

function maybe_serialize($data)
{
    if (is_array($data) || is_object($data))
        return serialize($data);

    // Double serialization is required for backward compatibility.
    // See https://core.trac.wordpress.org/ticket/12930
    // Also the world will end. See WP 3.6.1.
    if (is_serialized($data, false))
        return serialize($data);

    return $data;
}

/**
 * Unserialize value only if it was serialized.
 *
 * @since 2.0.0
 *
 * @param string $original Maybe unserialized original, if is needed.
 * @return mixed Unserialized data can be any type.
 */
function maybe_unserialize($original)
{
    if (is_serialized($original)) // don't attempt to unserialize data that wasn't serialized going in
        return @unserialize($original);
    return $original;
}

/**
 * Check value to find if it was serialized.
 *
 * If $data is not an string, then returned value will always be false.
 * Serialized data is always a string.
 *
 * @since 2.0.5
 *
 * @param string $data Value to check to see if was serialized.
 * @param bool $strict Optional. Whether to be strict about the end of the string. Default true.
 * @return bool False if not serialized and true if it was.
 */
function is_serialized($data, $strict = true)
{
    // if it isn't a string, it isn't serialized.
    if (!is_string($data)) {
        return false;
    }
    $data = trim($data);
    if ('N;' == $data) {
        return true;
    }
    if (strlen($data) < 4) {
        return false;
    }
    if (':' !== $data[1]) {
        return false;
    }
    if ($strict) {
        $lastc = substr($data, -1);
        if (';' !== $lastc && '}' !== $lastc) {
            return false;
        }
    } else {
        $semicolon = strpos($data, ';');
        $brace = strpos($data, '}');
        // Either ; or } must exist.
        if (false === $semicolon && false === $brace)
            return false;
        // But neither must be in the first X characters.
        if (false !== $semicolon && $semicolon < 3)
            return false;
        if (false !== $brace && $brace < 4)
            return false;
    }
    $token = $data[0];
    switch ($token) {
        case 's' :
            if ($strict) {
                if ('"' !== substr($data, -2, 1)) {
                    return false;
                }
            } elseif (false === strpos($data, '"')) {
                return false;
            }
        // or else fall through
        case 'a' :
        case 'O' :
            return (bool)preg_match("/^{$token}:[0-9]+:/s", $data);
        case 'b' :
        case 'i' :
        case 'd' :
            $end = $strict ? '$' : '';
            return (bool)preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
    }
    return false;
}


function get_meta($id, $key, $table_meta, $table_id_val)
{
    $ci =& get_instance();
    if (!empty($id) && !empty($key)) {
        $query = $ci->db->get_where($table_meta, array(
            $table_id_val => $id,
            'key' => $key,
        ))->row();
        $query = maybe_null_or_empty($query, 'value', true);
        return maybe_unserialize($query);
    }
}

function convert_date_to_french($date)
{
    if ($date && is_string($date) && strpos($date, '-'))
        return date('d/m/Y', strtotime($date));
}

function delete_in_table($table_name, $id, $redirect = '')
{
    $ci =& get_instance();
    $ci->db->delete($table_name, array('id' => $id));
    if ($redirect != '') {
        set_flashdata(lang('delete-success'), 'success');
        redirect($redirect);
    }
}

function control_unique_on_update($value, $db_field)
{
    $ci = &get_instance();
    $db_field = explode('.', $db_field);
    $table = $db_field[0];
    $target_field = $db_field[1];
    $id_value = $db_field[2];
    $query = $ci->db->query("SELECT id FROM $table where $target_field='$value'")->row();
    if ($queryId = maybe_null_or_empty($query, 'id')) {
        if ($id_value != $queryId) {
            $ci->form_validation->set_message('is_unique_on_update', "{field} existe déjà dans un autre champ {field} ");
            return false;
        }
    }
    return true;
}

function getDateByTime($time, $dateFormat = 'd/m/Y')
{
    if (!$time) {
        return '';
    }
    $time = (int)$time;
    return date($dateFormat, $time);
}

function user_can($group_name, $userID = null)
{
    $ci = &get_instance();
    return $ci->ion_auth->in_group($group_name, $userID);
}

function echoResponse($data)
{
    echo json_encode($data);
    die();
}

function sendMail($default = 'infos@abakescrafts.com', $args, $options = [])
{
    //ini_set("SMTP", "smtp.abakescrafts.com");
    $ci =& get_instance();
    if (empty($options)) {
        $options = $ci->setting_model->get_options();
    }
    $message = mailTemplateHtml($args, $options);
    $headers = "MIME-Version: 1.0 \n";

    $headers .= "Content-type: text/html; charset=iso-8859-1 \n";

    $headers .= "From: $default \n";
    $headers .= "Disposition-Notification-To: $default \n";
    $headers .= "X-Priority: 1  \n";

    $headers .= "X-MSMail-Priority: High \n";
    @mail($args['destination'], $args['title'], $message, $headers);

    /*$ci->load->library('email');
    $config['mailtype'] = 'html';
    $config['smtp_host'] = 'abakescrafts.com';
    $config['protocol'] = 'smtp';
    $config['smtp_port'] = 465;
    $config['smtp_user'] = 'infos@abakescrafts.com';
    $config['smtp_pass'] = '@NH]dRJ.FCpAw^NPy';
    $config['validate'] = true;

    $ci->email->initialize($config);
    $message = mailTemplateHtml($args, $options);
    $ci->email->from($default, lang('team').' '. $options['siteName']);
    $ci->email->to($args['destination']);
    $ci->email->subject($args['title']);
    $ci->email->message($message);
    $ci->email->send();
//    var_dump($args['destination']);exit;
    var_dump($ci->email->print_debugger());exit;*/
}

function mailTemplateHtml($args, $options)
{
    $path = get_upload_path();
    ob_start();
    ?>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml"
          style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
    <head>
        <meta name="viewport" content="width=device-width"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title><?php echo $args['title'] ?></title>


        <style type="text/css">
            img {
                max-width: 100%;
            }

            body {
                -webkit-font-smoothing: antialiased;
                -webkit-text-size-adjust: none;
                width: 100% !important;
                height: 100%;
                line-height: 1.6em;
            }

            body {
                background-color: #f6f6f6;
            }

            @media only screen and (max-width: 640px) {
                body {
                    padding: 0 !important;
                }

                h1 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h2 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h3 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h4 {
                    font-weight: 800 !important;
                    margin: 20px 0 5px !important;
                }

                h1 {
                    font-size: 22px !important;
                }

                h2 {
                    font-size: 18px !important;
                }

                h3 {
                    font-size: 16px !important;
                }

                .container {
                    padding: 0 !important;
                    width: 100% !important;
                }

                .content {
                    padding: 0 !important;
                }

                .content-wrap {
                    padding: 10px !important;
                }

                .invoice {
                    width: 100% !important;
                }
            }
        </style>
    </head>

    <body style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;"
          bgcolor="#f6f6f6">

    <table class="body-wrap"
           style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;"
           bgcolor="#f6f6f6">
        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                valign="top"></td>
            <td class="container" width="600"
                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;"
                valign="top">
                <div class="content"
                     style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
                    <table class="main" width="100%" cellpadding="0" cellspacing="0" itemprop="action" itemscope
                           style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; margin: 0; border: none;"
                    >
                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                            <td class="content-wrap"
                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;padding: 30px;border: 3px solid #777edd;border-radius: 7px; background-color: #fff;"
                                valign="top">
                                <meta itemprop="name" content="<?php echo $args['title'] ?>"
                                      style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;"/>
                                <table width="100%" cellpadding="0" cellspacing="0"
                                       style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                    <tr>
                                        <td style="text-align: center">
                                            <a href="#" style="display: block;margin-bottom: 10px;"> <img
                                                        src="<?= $path . $options['siteLogo'] ?>" height="150"
                                                        alt=""/></a> <br/>
                                        </td>
                                    </tr>
                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block"
                                            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                                            valign="top">
                                            <?php echo $args['message'] ?>
                                        </td>
                                    </tr>
                                    <?php
                                    if (isset($args['btnLink']) && isset($args['btnLabel'])) {
                                        ?>
                                        <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                            <td class="content-block" itemprop="handler" itemscope
                                                style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                                                valign="top">
                                                <a target="_blank" href="<?= $args['btnLink'] ?>" class="btn-primary"
                                                   itemprop="url"
                                                   style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #02c0ce; margin: 0; border-color: #02c0ce; border-style: solid; border-width: 8px 16px;"><?= $args['btnLabel'] ?></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>


                                    <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                        <td class="content-block"
                                            style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;"
                                            valign="top">
                                            &mdash; <b><?= lang('cordially') ?></b>
                                            - <?= lang('team') ?> <?php echo $options['siteName'] ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <div class="footer"
                         style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
                        <table width="100%"
                               style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="aligncenter content-block"
                                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;"
                                    align="center" valign="top"><?php echo $options['siteInfo'] ?>
                                </td>
                            </tr>
                            <tr style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                                <td class="aligncenter content-block"
                                    style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;"
                                    align="center" valign="top"><?php echo $options['siteDescription'] ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
            <td style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;"
                valign="top"></td>
        </tr>
    </table>
    </body>

    </html>


    <?php
    return ob_get_clean();
}
