<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 06/06/2019
 * Time: 10:10
 */
require(APPPATH . '/libraries/REST_Controller.php');
class Problemreport extends \Restserver\Libraries\REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('problemreport_model');
        $this->load->library('form_validation');
    }

    public function set_post(){
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
            'message'=>$this->post('message'),
            'screenshot'=>$this->post('screenshot'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'message',
                'label'=>"Message",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'screenshot',
                'label'=>"Capture",
                'rules'=>"trim|required|valid_base64",
            ],
        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            unset($data['username'], $data['password']);
            $data['user_id']=maybe_null_or_empty($validation['data'],'id');
            if(maybe_null_or_empty($data, 'screenshot')){
                $files['screenshot'] = getBase64UploadFileNameAndFileData($data['screenshot']);
                if (!empty($files['screenshot'])) {
                    $data['screenshot'] = maybe_null_or_empty($files['screenshot'], 'file_name');
                }
            }
            $this->problemreport_model->insert($data);
            $this->response([
                'status'=>true,
                'message'=>'Problème signalé reçu. Nous nous pencherons sur ce cas et vous ferons un retour'
            ], null, true);
            megaUploadAndOrExit($files);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }

}