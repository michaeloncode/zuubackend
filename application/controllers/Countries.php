<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 07/03/2019
 * Time: 15:48
 */

require(APPPATH . '/libraries/REST_Controller.php');
class Countries extends \Restserver\Libraries\REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_model');
    }

    public function all_get(){
        $this->response([
            'status'=>true,
            'data'=>$this->get('forSelect') ? $this->country_model->forSelect2() : $this->country_model->get()
        ]);
    }

    public function cities_get(){
        if($codeIso3 = $this->get('iso3')){
            $this->response([
                'status'=>true,
                'data'=>$this->country_model->getCitiesByCountry($codeIso3)
            ]);
        }

    }

    public function forSelect_get(){
        $countries = $this->country_model->forSelect2();
        if(!empty($countries)){
            $this->response([
                'status'=>true,
                'data'=>$countries
            ]);
        }
        $this->response([
            'status'=>false
        ]);
    }
}