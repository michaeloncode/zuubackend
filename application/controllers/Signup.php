<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 09/01/2019
 * Time: 12:49
 */
require(APPPATH . '/libraries/REST_Controller.php');
class Signup extends \Restserver\Libraries\REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('signup');
        $this->load->model('user_model');
    }

    public function set_post(){
        $data=[
            'last_name'=>$this->post('last_name'),
            'first_name'=>$this->post('first_name'),
            'email'=>$this->post('email'),
            'phone'=>$this->post('phone'),
            'password'=>$this->post('password'),
        ];
        //$this->response($data);
        $this->form_validation->set_data($data);
        setFormValidationRules([
            [
                'name'=>'last_name',
                'label'=>'Nom',
                'rules'=>'trim|required'
            ],
            [
                'name'=>'first_name',
                'label'=>'Prénom(s)',
                'rules'=>'trim|required'
            ],
            [
                'name'=>'email',
                'label'=>'Email',
                'rules'=>'trim|required|valid_email|is_unique[users.email]'
            ],
            [
                'name'=>'phone',
                'label'=>'Téléphone',
                'rules'=>'trim|required|is_unique[users.phone]'
            ],
            [
                'name'=>'password',
                'label'=>'Mot de passe',
                'rules'=>'trim|required'
            ],

        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            if($userData = $this->user_model->insert($data)){
                //TODO send mail
                /*senditMail();
                $this->response([
                    'status'=>false,
                    'data'=>$this->email->print_debugger()
                ]);*/
                $this->response([
                    'status'=>true,
                    'data'=>$this->user_model->getCurrentUser(maybe_null_or_empty($userData, 'id'))
                ]);
            }
            $this->response([
                'status'=>false,
                'message'=>'Erreur rencontrée. Veuillez réessayer'
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);

    }

    public function confirm_post(){
        $data=[
            'confirm_code'=>$this->post('confirm_code'),
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
        ];
        $this->form_validation->set_data($data);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>'trim|required'
            ],

            [
                'name'=>'confirm_code',
                'label'=>'Code de confirmation',
                'rules'=>'trim|required|is_natural_no_zero'
            ],
        ]);
        if($this->form_validation->run()){
            $validation = $this->user_model->validate($data['username'], $data['password'], false);
            if(maybe_null_or_empty($validation, 'status')!== true){
                $this->response($validation);
            }else{
                $user=maybe_null_or_empty($validation, 'data');
                $this->response($this->user_model->confirm_user(maybe_null_or_empty($user, 'id'), $data['confirm_code'], maybe_null_or_empty($user, 'activation_code')));
            }

        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }
}