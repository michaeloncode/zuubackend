<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 25/05/2019
 * Time: 13:52
 */
require(APPPATH . '/libraries/REST_Controller.php');
class Payment extends \Restserver\Libraries\REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');
    }

    public function getAllMeans_get(){
        $this->response([
            'status'=>true,
            'data'=>$this->payment_model->getAll()
        ]);
    }
}