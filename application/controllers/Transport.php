<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 23/04/2019
 * Time: 07:02
 */

require(APPPATH . '/libraries/REST_Controller.php');

class Transport extends \Restserver\Libraries\REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('transport_model');
    }

    public function all_get(){
        $this->response([
            'status'=>true,
            'data'=>$this->transport_model->getTransportMeans()
        ]);
    }

    public function getUserTransportMeans_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>'trim|required'
            ],

        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            $this->response([
                'status'=>true,
                'data'=>$this->transport_model->getUserTransportMeansByUserID(maybe_null_or_empty($validation['data'], 'id'))
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function setUserTransportMean_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
            'transport_mean_id'=>$this->post('transport_mean_id'),
            'state'=>$this->post('state'),
            'brand'=>$this->post('brand'),
            'model'=>$this->post('model'),
            'places_available'=>$this->post('places_available'),
            'transport_pic'=>$this->post('transport_pic'),
            'assurance_pic'=>$this->post('assurance_pic'),
            'driver_licence_pic'=>$this->post('driver_licence_pic'),
            'matriculation'=>$this->post('matriculation'),
            'color'=>$this->post('color'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'transport_mean_id',
                'label'=>"ID Moyen de transport",
                'rules'=>'trim|required|is_natural_no_zero'
            ],
            [
                'name'=>'state',
                'label'=>"Etat",
                'rules'=>'trim|required|is_natural_no_zero'
            ],
            [
                'name'=>'brand',
                'label'=>"Marque",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'model',
                'label'=>"Modèle",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'places_available',
                'label'=>"Nombre de places disponible",
                'rules'=>'trim|required|is_natural_no_zero'
            ],
            [
                'name'=>'transport_pic',
                'label'=>"Image du moyen de transport",
                'rules'=>'trim|required|valid_base64'
            ],[
                'name'=>'assurance_pic',
                'label'=>"Image Assurance",
                'rules'=>'trim|required|valid_base64'
            ],
            [
                'name'=>'driver_licence_pic',
                'label'=>"Image Permis de conduire",
                'rules'=>'trim|required|valid_base64'
            ],
            [
                'name'=>'matriculation',
                'label'=>"Immatriculation",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'color',
                'label'=>"Couleur",
                'rules'=>'trim|required'
            ],

        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            unset($data['password'],$data['username']);
            if(maybe_null_or_empty($data, 'transport_pic')){
                $files['transport_pic'] = getBase64UploadFileNameAndFileData($data['transport_pic'], getKnownImageExtensions());
                if (!empty($files['transport_pic'])) {
                    $data['transport_pic'] = maybe_null_or_empty($files['transport_pic'], 'file_name');
                }
            }
            if(maybe_null_or_empty($data, 'assurance_pic')){
                $files['assurance_pic'] = getBase64UploadFileNameAndFileData($data['assurance_pic'], getKnownImageExtensions());
                    if (!empty($files['assurance_pic'])) {
                        $data['assurance_pic'] = maybe_null_or_empty($files['assurance_pic'], 'file_name');
                    }
            }
            if(maybe_null_or_empty($data, 'driver_licence_pic')){
                $files['driver_licence_pic'] = getBase64UploadFileNameAndFileData($data['driver_licence_pic'], getKnownImageExtensions());
                    if (!empty($files['driver_licence_pic'])) {
                        $data['driver_licence_pic'] = maybe_null_or_empty($files['driver_licence_pic'], 'file_name');
                    }
            }
            $data['user_id']=maybe_null_or_empty($validation['data'], 'id');
            $data['created_at']=date('Y-m-d G:i:s');
            $transportID = $this->transport_model->insertUserTransportMean($data);
            if($transportID){
                $this->response([
                    'status'=>true,
                    'data'=>$data
                ], null, true);
                //response to be continued
                megaUploadAndOrExit($files);
            }
            $this->response([
                'status' => false,
                'message' => 'Something went wrong'
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function setTransportOfferProposal_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
            'starting_point'=>$this->post('starting_point'),
            'starting_point_gps_lat'=>$this->post('starting_point_gps_lat'),
            'starting_point_gps_long'=>$this->post('starting_point_gps_long'),
            'starting_date'=>$this->post('starting_date'),
            'end_point'=>$this->post('end_point'),
            'end_point_gps_lat'=>$this->post('end_point_gps_lat'),
            'end_point_gps_long'=>$this->post('end_point_gps_long'),
            'end_date'=>$this->post('end_date'),
            'transport_mean_for_user_id'=>$this->post('transport_mean_for_user_id'),
            'price'=>$this->post('price'),
            'offer_id'=>$this->post('offer_id')
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>'trim|required'
            ],[
                'name'=>'offer_id',
                'label'=>"ID Offre de transport",
                'rules'=>'trim'
            ],
            [
                'name'=>'starting_point',
                'label'=>"Lieu de depart",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'starting_point_gps_lat',
                'label'=>"GPS Latitude Lieu de départ",
                'rules'=>'trim'
            ],
            [
                'name'=>'starting_point_gps_long',
                'label'=>"GPS Longitude Lieu de départ",
                'rules'=>'trim'
            ],
            [
                'name'=>'starting_date',
                'label'=>"Date de démarrage",
                'rules'=>'trim|required|is_natural_no_zero'
            ],
            [
                'name'=>'end_point',
                'label'=>"GPS Longitude Lieu de départ",
                'rules'=>'trim'
            ],
            [
                'name'=>'end_point_gps_lat',
                'label'=>"GPS Latitude Lieu de départ",
                'rules'=>'trim'
            ],
            [
                'name'=>'end_point_gps_long',
                'label'=>"GPS Longitude Arrivée",
                'rules'=>'trim'
            ],
            [
                'name'=>'end_date',
                'label'=>"Date d'arrivée",
                'rules'=>'trim|required|is_natural_no_zero'
            ],
            [
                'name'=>'transport_mean_for_user_id',
                'label'=>"ID Moyen de transport utilisateur",
                'rules'=>'trim|required|is_natural_no_zero'
            ],
            [
                'name'=>'price',
                'label'=>"Prix",
                'rules'=>'trim|required|is_natural_no_zero'
            ],
        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            unset($data['username'], $data['password']);
            $offerID=maybe_null_or_empty($data, 'offer_id', true);
            unset($data['offer_id']);
            //if offerID exist
            if($offerID){
                $offerID = (int) $offerID;
                //verify if the passed offer_id corresponds to the current user
                if($this->transport_model->isOfferTransportationProposalForUser(maybe_null_or_empty($validation['data'], 'id'), $offerID)){
                    $this->transport_model->updateOfferTransportationProposal($offerID, $data);
                    $this->response([
                        'status'=>true,
                        'data'=>$this->transport_model->getOfferTransportProposal($offerID),
                        'message'=>'Offre de transport mis à jour avec succès'
                    ]);
                }else{
                    $this->response([
                        'status'=>false,
                        'message'=>'Action non authorisée'
                    ], 403);
                }
            }
            //verify the passed transport_mean_for_user_id parameter if  it is for user
            if($this->transport_model->isTransportMeanForUser(maybe_null_or_empty($validation['data'], 'id'), $data['transport_mean_for_user_id'])){
                $offerID=$this->transport_model->insertOfferTransportationProposal($data);
                $this->response([
                    'status'=>true,
                    'data'=>$this->transport_model->getOfferTransportProposal($offerID),
                    'message'=>'Offre de transport soumis avec succès'
                ]);
            }
            $this->response([
                'status'=>false,
                'message'=>'Action non authorisée'
            ], 403);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function removeTransportOfferProposal_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
            'offer_id'=>$this->post('offer_id')
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'offer_id',
                'label' => 'ID Offre de transport',
                'rules' => 'trim|required|is_natural_no_zero',
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            if($this->transport_model->isOfferTransportationProposalForUser(maybe_null_or_empty($validation['data'], 'id'), $data['offer_id'])){
                $this->transport_model->updateOfferTransportationProposal($data['offer_id'], ['active'=>0]);
                $this->response([
                    'status'=>true,
                    'message'=>'Offre de transport supprimé'
                ]);
            }
            $this->response([
                'status'=>false,
                'message'=>'Action non autorisée'
            ], 403);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function getUserTransportOfferProposals_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            $this->response([
                'status'=>true,
                'data'=>$this->transport_model->getOfferTransportationProposalByUserID(maybe_null_or_empty($validation['data'], 'id'))
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function getActiveTransportOfferProposals_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            $this->response([
                'status'=>true,
                'data'=>$this->transport_model->getActiveOfferTransportationProposals()
            ]);
            //TODO afficher en premier les utilisateurs abonnés par l'utilisateur en cours
            //TODO afficher en fonction des préférences de l'utilisateur en cours
            //TODO afficher les offres ne s'expirant pas avant 5min


        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }
}