<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 17/01/2019
 * Time: 14:14
 */

require(APPPATH . '/libraries/REST_Controller.php');
class Risks extends \Restserver\Libraries\REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('risk_model');
    }

    public function all_get(){
        $this->response([
            'status'=>true,
            'data'=>$this->risk_model->get()
        ]);
    }

}