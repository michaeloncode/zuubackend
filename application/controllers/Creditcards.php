<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 03/06/2019
 * Time: 16:16
 */

require(APPPATH . '/libraries/REST_Controller.php');
class Creditcards extends \Restserver\Libraries\REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->_minYearForExpiringYear = date('Y')-4;
        $this->load->model('creditcard_model');
        $this->load->library('form_validation');
    }


    public function set_post(){
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
            'card_number'=>$this->post('card_number'),
            'owner'=>$this->post('owner'),
            'expiring_month'=>$this->post('expiring_month'),
            'expiring_year'=>$this->post('expiring_year'),
            'cvv'=>$this->post('cvv'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'card_number',
                'label'=>"Numéro de carte de crédit",
                'rules'=>"trim|required|is_natural_no_zero",
            ],
            [
                'name'=>'owner',
                'label'=>"Nom du proprietaire",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'expiring_month',
                'label'=>"Mois d'expiration de carte de crédit",
                'rules'=>"trim|required|is_natural_no_zero|max_length[2]",
            ],
            [
                'name'=>'expiring_year',
                'label'=>"Année d'expiration de carte de crédit",
                'rules'=>"trim|required|is_natural_no_zero|exact_length[4]|greater_than[$this->_minYearForExpiringYear]",
                'custom_message'=>[
                    'greater_than'=>"L'année d'expiration saisie doit dépasser l'année 2015"
                ]
            ],
            [
                'name'=>'cvv',
                'label'=>"CVV de carte de crédit",
                'rules'=>"trim|required|is_natural_no_zero|exact_length[3]",
            ],
        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            unset($data['username'], $data['password']);
            $data['user_id']=maybe_null_or_empty($validation['data'], 'id');
            $this->creditcard_model->insertOrUpdate($data);
            $this->response([
                'status'=>true,
                'data'=>$this->creditcard_model->getByUserIDAndDecrypt($data['user_id'])
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }

    public function forUser_post(){
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>"trim|required",
            ],
        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            $this->response([
                'status'=>true,
                'data'=>$this->creditcard_model->getByUserIDAndDecrypt(maybe_null_or_empty($validation['data'], 'id'))
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }

    public function remove_post(){
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>"trim|required",
            ],
        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            $this->creditcard_model->deleteForUser(maybe_null_or_empty($validation['data'], 'id'));
            $this->response([
                'status'=>true,
                'message'=>'Carte de crédit supprimé avec succès'
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }
}