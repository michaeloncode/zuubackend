<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 31/12/2018
 * Time: 15:21
 */
require(APPPATH . '/libraries/REST_Controller.php');

class Login extends \Restserver\Libraries\REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function jset_post()
    {

        if (($identity = $this->post('email')) && ($password = $this->post('password'))) {
            $this->load->library('form_validation');
            $this->form_validation->set_data(array(
                'email'    =>  $identity,
                'password'=>$password
            ));
            setFormValidationRules([
                [
                    'name'=>'email',
                    'label'=>'Email',
                    'rules'=>'trim|required|valid_email'
                ],
                [
                    'name'=>'password',
                    'label'=>'Mot de passe',
                    'rules'=>'trim|required'
                ]
            ]);
            if($this->form_validation->run()){
                if ($this->ion_auth->login($identity, $password, false)) {
                    $userData = $this->user_model->getCurrentUser();
                    $this->ion_auth->logout();
                    $this->response([
                        'status' => true,
                        'data' => $userData,
                        'message' => 'Utilisateur connecté'
                    ]);
                }
                $this->response([
                    'status' => false,
                    'message' => "Email ou mot de passe incorrect"
                ]);
            }
            $this->form_validation->set_error_delimiters(null, null);
            $this->response([
                'status' => false,
                'message' => $this->form_validation->error_string()
            ]);
        }
        $this->response([
            'status' => false,
            'message' => 'Données invalides'
        ]);
    }

    public function set_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data([
            'phone'=>$phone=$this->post('phone'),
            'country'=>$country=$this->post('country')
        ]);
        setFormValidationRules([
            [
                'name'=>'phone',
                'label'=>'Téléphone',
                'rules'=>'trim|required|is_natural_no_zero'
            ],
            [
                'name'=>'country',
                'label'=>'Pays',
                //country ID OR iso 3 is sent
                'rules'=>'trim|required'
            ],

        ]);
        if($this->form_validation->run()){
            //TODO send temporary confirmation code
            if($userID = $this->user_model->checkPhoneNumber($phone)){
                $this->user_model->update_meta($userID, 'confirm_code', getRandomCode());
                $this->response([
                    'status'=>true,
                    'data'=>$this->user_model->getCurrentUser($userID)
                ]);
            }else{
                //TODO user phone number not in DB
                $data['first_name']='Zuu New User';
                $data['last_name']='Zuu New User';
                $data['phone']=$phone;
                $data['country']=$country;
                $data['isProfilModified']=false; //is a newly created user and profil informations are not yet modified
                $userData = $this->user_model->insert($data);
                //Userdata contains ID, activation code, email etc
                if(!empty($userData)){
                    $this->response([
                        'status'=>true,
                        'data'=>$this->user_model->getCurrentUser(maybe_null_or_empty($userData, 'id'))
                    ]);
                }
            }
            $this->response([
                'status'=>false,
                'message'=>'Erreur rencontrée. Veuillez réessayer'
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }

    public function withPassword_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>'trim|required'
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>'trim|required'
            ]
        ]);
        if($this->form_validation->run()){
            $validation = $this->user_model->validate($data['username'], $data['password'], true, false);
            $this->response($validation);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }
}