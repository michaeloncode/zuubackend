<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 05/06/2019
 * Time: 17:30
 */

require(APPPATH . '/libraries/REST_Controller.php');
class Contactform extends \Restserver\Libraries\REST_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('contactform_model');
        $this->load->library('form_validation');
    }

    public function set_post(){
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
            'subject'=>$this->post('subject'),
            'message'=>$this->post('message'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'subject',
                'label'=>"Sujet",
                'rules'=>"trim|required|max_length[255]",
            ],
            [
                'name'=>'message',
                'label'=>"Message",
                'rules'=>"trim|required",
            ],
        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            unset($data['username'], $data['password']);
            $data['user_id']=maybe_null_or_empty($validation['data'],'id');
            $this->contactform_model->insert($data);
            $this->response([
                'status'=>true,
                'message'=>'Merci de nous avoir contacté. Vous aurez un retour bientôt'
            ]);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);
    }
}