<?php
/**
 * Created by PhpStorm.
 * User: MikOnCode
 * Date: 31/12/2018
 * Time: 12:29
 */
require(APPPATH . '/libraries/REST_Controller.php');

class Users extends \Restserver\Libraries\REST_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function all_get()
    {
        $this->response([
            'status' => true,
            'data' => $this->ion_auth->users()->result(),
        ]);
    }

    public function exist_get()
    {
        //maybe number or email
        $data = [
            'identity' => $this->get('identity')
        ];
        $this->load->library('form_validation');
        $this->form_validation->set_data($data);
        setFormValidationRules([
            [
                'name' => 'identity',
                'label' => 'Identité',
                'rules' => 'trim|required'
            ]
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            if (is_numeric($data['identity'])) {
                $user = $this->user_model->userExistByField($data['identity'], 'phone');
            } else {
                $user = $this->user_model->userExistByField($data['identity'], 'email');
            }
            if ($user) {
                $this->response([
                    'status' => true,
                    'data' => $user
                ]);
            }
            $this->response([
                'status' => false,
                'message' => 'Utilisateur non existant'
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function mailExist_get()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data([
            'email' => $email = $this->get('identity')
        ]);
        setFormValidationRules([
            [
                'name' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email',
            ]
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $this->response([
                'status' => true,
                'data' => $this->user_model->mailExist($email)
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function userNameExist_get()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data([
            'username' => $username = $this->get('identity')
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ]
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $this->response([
                'status' => true,
                'data' => $this->user_model->userNameExist($username)
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function phoneNumberExist_get()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data([
            'phone' => $phone = $this->get('identity')
        ]);
        setFormValidationRules([
            [
                'name' => 'phone',
                'label' => "Numéro de téléphone",
                'rules' => 'trim|required|is_natural_no_zero',
            ]
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $this->response([
                'status' => true,
                'data' => $this->user_model->phoneExist($phone)
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function modifyUser_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'user_id' => $userID = (int)$this->post('user_id'),
            'first_name' => $this->post('first_name'),
            'last_name' => $this->post('last_name'),
            'sex' => $this->post('sex'),
            'email' => $this->post('email'),
            'city' => $this->post('city'),
            'username' => $this->post('username'),
            'userpic' => $this->post('userpic'),
            'old_username' => $this->post('old_username'),
            'password' => $this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name' => 'user_id',
                'label' => 'ID Utilisateur',
                'rules' => 'trim|required|is_natural_no_zero',
            ],
            [
                'name' => 'first_name',
                'label' => 'Prénom(s)',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'last_name',
                'label' => 'Nom',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'sex',
                'label' => 'Sexe',
                'rules' => 'trim|required|in_list[M,F]',//M for male and F for Female
            ],
            [
                'name' => 'email',
                'label' => 'Email',
                'rules' => "trim|required|valid_email|callback_is_unique_on_update[users.email.$userID]",
            ],
            [
                'name' => 'city',
                'label' => 'Ville',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required|is_unique[users.username]',
                'custom_message'=>[
                    'is_unique'=>"Ce nom d'utilisateur existe déjà"
                ]
            ],
            [
                'name' => 'old_username',
                'label' => "Nom d'utilisateur actuel",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => "Mot de passe",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'userpic',
                'label' => "Photo de profil",
                'rules' => 'trim|required',
            ],
        ]);

        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            //$this->response($data);
            $validateUser = $this->user_model->validate($data['old_username'], $data['password'], false);
            unset($data['old_username'], $data['password'], $data['user_id']);
            if (maybe_null_or_empty($validateUser, 'status') && ($userID==maybe_null_or_empty($validateUser['data'], 'id'))) {
                if (maybe_null_or_empty($data, 'userpic')) {
                    /*$file = uploadFileDataWithFilePutContent($data['userpic']);
                    if (!empty($file)) {
                        $data['userpic'] = maybe_null_or_empty($file, 'file_name');
                    }*/
                    $data['userpic'] = uploadBase64($data['userpic']);
                } else {
                    unset($data['userpic']);
                }
                $data['isProfilModified'] = true;
                $this->user_model->update($validateUser['data']->id, $data);
                $this->response([
                    'status' => true,
                    'data' => $this->user_model->getCurrentUser($validateUser['data']->id)
                ]);

                if (!empty($file)) {
                    uploadFileDataWithFilePutContent(maybe_null_or_empty($file, 'file'), maybe_null_or_empty($file, 'data'));
                }
                exit;
            } else {
                $this->response([
                    'status' => false,
                    'message' => maybe_null_or_empty($validateUser, 'message')
                ]);
            }
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);

    }

    public function modifyPassword_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('old_password'),
            'new_password' => $this->post('new_password'),
        ]);
        setFormValidationRules([

            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'new_password',
                'label' => 'Nouveau mot de passe',
                'rules' => 'trim|required',
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            } else {
                $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'pending_password', $data['new_password']);
                $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'password_modification_code', $password_code = getRandomCode());
                $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'isPasswordConfirmed', false);
                $this->response([
                    'status' => true,
                    'message' => 'Un mail contenant un code de confirmation vous a été envoyé',
                    'data' => $this->user_model->getCurrentUser(maybe_null_or_empty($validation['data'], 'id'))
                ]);
                //TODO send password code to user email
            }
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function confirmPasswordCode_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('old_password'),
            'code' => $this->post('password_code'),
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'code',
                'label' => 'Code de confirmation',
                'rules' => 'trim|required|is_natural_no_zero',
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            } else {
                if (!$this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'), 'isPasswordConfirmed')) {
                    if($this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'), 'password_modification_code') == $data['code']){
                        $this->ion_auth->update(maybe_null_or_empty($validation['data'], 'id'), array(
                            'password' => $this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'), 'pending_password')
                        ));
                        $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'pending_password', null);
                        $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'password_modification_code', null);
                        $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'isPasswordModified', true);
                        $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'isPasswordConfirmed', true);
                        $this->response([
                            'status'=>true,
                            'data'=>$this->user_model->getCurrentUser(maybe_null_or_empty($validation['data'], 'id')),
                            'message'=>'Mot de passe modifié avec succès'
                        ]);
                    }else{
                        $this->response([
                            'status'=>false,
                            'message'=>'Code de confirmation incorrect'
                        ]);
                    }
                }else{
                    $this->response([
                        'status'=>false,
                        'message'=>'Mot de passe déjà confirmé'
                    ]);
                }
            }
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function setIDCardPic_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username'=>$this->post('username'),
            'password'=>$this->post('password'),
            'id_card_pic'=>$this->post('id_card_pic'),
        ]);
        setFormValidationRules([
            [
                'name'=>'username',
                'label'=>"Nom d'utilisateur",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'password',
                'label'=>"Mot de passe",
                'rules'=>"trim|required",
            ],
            [
                'name'=>'id_card_pic',
                'label'=>"Photo de pièces d'identité",
                'rules'=>"trim|required|valid_base64",
            ],
        ]);
        if($this->form_validation->run()){
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if(!$validation['status']){
                $this->response($validation);
            }
            if(maybe_null_or_empty($data, 'id_card_pic')){
                $files['id_card_pic'] = getBase64UploadFileNameAndFileData($data['id_card_pic']);
                if (!empty($files['id_card_pic'])) {
                    $data['id_card_pic'] = maybe_null_or_empty($files['id_card_pic'], 'file_name');
                    $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'id_card_pic', $data['id_card_pic']);
                    $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'idCardControl', 'waiting');
                    //TODO come back for userAccountIsValid
                    $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'userAccountIsValid', true);
                }
            }
            $this->response([
                'status'=>true,
                'message'=>"Carte d'identité soumise avec succès. Vous serez notifiés une fois validée"
                //'data'=>$this->user_model->getCurrentUser(maybe_null_or_empty($validation['data'], 'id'))
            ], null, true);
            megaUploadAndOrExit($files);
        }
        $this->response([
            'status'=>false,
            'message'=>setErrorDelimiter()
        ]);

    }

    public function validateBySelfie_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
            'selfie'=>$this->post('selfie')
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'selfie',
                'label' => 'Selfie',
                'rules' => 'trim|required|valid_base64',
            ],

        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            if($this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'),'selfiePicControl')=='waiting'){
                $this->response([
                    'status'=>false,
                    'message'=>"Vous avez déja soumis votre selfie d'activation et votre demande est en cours d'analyse"
                ]);
            }
            if($this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'),'selfieGenerationStarted')){
                //if activation code time interval has not yet done 5min
                $elapsedInterval=abs((int)$this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'),'generatedSelfieVerificationDate') - time()) / 60;
                //$this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'selfieGenerationStarted', null);
                if($elapsedInterval<5){
                    if(maybe_null_or_empty($data, 'selfie')){
                        $files['selfie'] = getBase64UploadFileNameAndFileData($data['selfie'], getKnownImageExtensions());
                        if (!empty($files['selfie'])) {
                            $selfieUrl = maybe_null_or_empty($files['selfie'], 'file_name');
                            $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'selfieUrl', $selfieUrl);
                            $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'selfiePicControl', 'waiting');
                            $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'selfieSubmittedDate', time());
                            $this->user_model->delete_meta(maybe_null_or_empty($validation['data'],'id'), 'selfieGenerationStarted');
                        }
                    }
                    $this->response([
                        'status'=>true,
                        'data'=>$this->user_model->getCurrentUser(maybe_null_or_empty($validation['data'], 'id'), ['selfieUrl', 'isSelfieSubmitted']),
                        'message'=>"Demande d'activation reçue. Un modérateur de l'équipe ZUU prendra en charge votre demande. Vous recevrez une notification une fois terminé"
                    ], null, true);
                    megaUploadAndOrExit($files);
                }
                $this->response([
                    'status' => false,
                    'message' => "Demande d'activation expirée. Veuillez refaire une nouvelle demande"
                ]);
            }
            $this->response([
                'status' => false,
                'message' => "Aucune demande préalable de code de vérification n'a été effectuée"
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);

    }

    private function getUserCode($userLastName, $userLastNamelength=4, $overAllLength=10, $permittedChars = '1234567890ABCDEFGHIJKLMPQRSTUVWXYZ'){
        return strtoupper(substr(preg_replace('/[^a-zA-Z0-9-_\.]/','', substr($userLastName,0, $userLastNamelength)).'-'.str_shuffle($permittedChars), 0, $overAllLength));
    }

    public function generateSelfieValidationCode_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            if($this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'),'selfiePicControl')=='waiting'){
                $this->response([
                    'status'=>false,
                    'message'=>"Vous avez déja soumis votre selfie d'activation et votre demande est en cours d'analyse"
                ]);
            }
            if($this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'),'selfieGenerationStarted')){
                //if activation code time interval has not yet done 5min
                $elapsedInterval=abs(($generatedDate = (int) $this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'),'generatedSelfieVerificationDate')) - time()) / 60;
                if($elapsedInterval<=5){
                    $this->response([
                        'status'=>false,
                        'data'=>[
                            'code'=>$this->user_model->get_meta(maybe_null_or_empty($validation['data'], 'id'),'generatedSelfieVerificationCode'),
                            'date'=>$generatedDate
                        ],
                        'message'=>"Votre code d'activation unique n'a pas encore expiré"
                    ]);
                }
            }
            $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'selfieGenerationStarted', true);
            $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'generatedSelfieVerificationCode', $codeGenerated=$this->getUserCode(maybe_null_or_empty($validation['data'], 'last_name')));
            $this->user_model->update_meta(maybe_null_or_empty($validation['data'], 'id'), 'generatedSelfieVerificationDate', $generatedDate=time());
            $this->response([
                'status'=>true,
                'data'=>[
                    'code'=>$codeGenerated,
                    'date'=>$generatedDate
                ]
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function modifyIndividualDataOnTheFly_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
            'field'=>$this->post('field'),
            'value'=>$this->post('value'),
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'field',
                'label' => 'Clé',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'value',
                'label' => 'Valeur',
                'rules' => 'trim|required',
            ],

        ]);
        if ($this->form_validation->run()) {
            $authorisedFields=[
                'first_name', 'last_name', 'username', 'sex', 'phone', 'email',
                'city', 'birthday', 'facebook_url', 'twitter_url', 'linkedin_url',
                'origin_city', 'school_info', 'company', 'profession', 'website',
                'language', 'religion', 'political_opinion', 'about'
            ];
            if(!in_array($data['field'], $authorisedFields)){
                $this->response([
                    'status'=>false,
                    'message'=>'Action non authorisée',
                ], 403);
            }
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            $updateData[$data['field']]=$data['value'];
            $message = 'Informations enregistrées avec succès';
            $criticalFields=[
                'first_name', 'last_name', 'username', 'phone', 'email', 'sex'
            ];
            //if field is critical and field value is different from user field value
            if(in_array($data['field'], $criticalFields) && ($data['value'] != maybe_null_or_empty($validation['data'], $data['field']))){
                $uniqueFields=[
                    'username', 'phone', 'email'
                ];
                if(in_array($data['field'], $uniqueFields)){
                    //unique field value is already available in the DB
                    if($this->user_model->userExistByField($data['value'], $data['field'], true)){
                        $this->response([
                            'status'=>false,
                            'message'=>"La valeur saisie n'est pas disponible. Veuillez changer et réessayer"
                        ]);
                    }
                }
                $updateData['userAccountIsValid']=false;
                $message.='\n Vous avez modifié des informations critiques concernant votre profil. Veuillez revalider votre compte';
            }
            $this->user_model->update(maybe_null_or_empty($validation['data'], 'id'), $updateData);
            $this->response([
                'status'=>true,
                'data'=>$this->user_model->getCurrentUser(maybe_null_or_empty($validation['data'], 'id')),
                'message'=>$message
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function is_unique_on_update($field_value, $args)
    {
        return control_unique_on_update($field_value, $args);
    }

    public function setUserNotes_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
            'identity_note' => $this->post('identity_note'),
            'paymentMean_note' => $this->post('paymentMean_note'),
            'profile_note' => $this->post('profile_note'),
            'firstName_note' => $this->post('firstName_note'),
            'lastName_note' => $this->post('lastName_note'),
            'username_note' => $this->post('username_note'),
            'sex_note' => $this->post('sex_note'),
            'phone_note' => $this->post('phone_note'),
            'email_note' => $this->post('email_note'),
            'birthday_note' => $this->post('birthday_note'),
            'company_note' => $this->post('company_note'),
            'profession_note' => $this->post('profession_note'),
            'facebook_note' => $this->post('facebook_note'),
            'twitter_note' => $this->post('twitter_note'),
            'linkedin_note' => $this->post('linkedin_note'),
            'language_note' => $this->post('language_note'),
            'religion_note' => $this->post('religion_note'),
            'about_note' => $this->post('about_note'),
            'rating_note' => $this->post('rating_note'),
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
            [
                'name' => 'identity_note',
                'label' => "Note pour contrôle d'identité",
                'rules' => 'trim|required|less_than_equal_to[20]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'paymentMean_note',
                'label' => "Note pour moyen de paiement",
                'rules' => 'trim|required|less_than_equal_to[20]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'profile_note',
                'label' => "Note pour profil",
                'rules' => 'trim|required|less_than_equal_to[20]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'firstName_note',
                'label' => "Note pour prénom",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'lastName_note',
                'label' => "Note pour nom",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'username_note',
                'label' => "Note pour nom d'utilisateur",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'sex_note',
                'label' => "Note pour sexe",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'phone_note',
                'label' => "Note pour téléphone",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'email_note',
                'label' => "Note pour email",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'birthday_note',
                'label' => "Note pour date de naissance",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'company_note',
                'label' => "Note pour entreprise",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'profession_note',
                'label' => "Note pour profession",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'facebook_note',
                'label' => "Note pour facebook",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'twitter_note',
                'label' => "Note pour twitter",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'linkedin_note',
                'label' => "Note pour linkedin",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'language_note',
                'label' => "Note pour langue",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'religion_note',
                'label' => "Note pour réligion",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'about_note',
                'label' => "Note pour a propos",
                'rules' => 'trim|required|less_than_equal_to[1]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
            [
                'name' => 'rating_note',
                'label' => "Note globale",
                'rules' => 'trim|required|less_than_equal_to[25]',
                'custom_message'=>[
                    'less_than_equal_to'=>"{field} ne doit pas excéder {param} point(s)"
                ]
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            unset($data['username'], $data['password']);
            $this->user_model->megaMetaUpdate(maybe_null_or_empty($validation['data'], 'id'), $data);
            $this->response([
                'status'=>true,
                'message'=>"Notes d'utilisateur enregistrées avec succès",
                'data'=>$this->user_model->getNotesMeta(maybe_null_or_empty($validation['data'], 'id'))
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }

    public function retrieveUserNotes_post(){
        $this->load->library('form_validation');
        $this->form_validation->set_data($data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
        ]);
        setFormValidationRules([
            [
                'name' => 'username',
                'label' => "Nom d'utilisateur",
                'rules' => 'trim|required',
            ],
            [
                'name' => 'password',
                'label' => 'Mot de passe actuel',
                'rules' => 'trim|required',
            ],
        ]);
        if ($this->form_validation->run()) {
            $this->load->model('user_model');
            $validation = $this->user_model->validate($data['username'], $data['password'], true);
            if (maybe_null_or_empty($validation, 'status') !== true) {
                $this->response($validation);
            }
            $this->response([
                'status'=>true,
                'data'=>$this->user_model->getNotesMeta(maybe_null_or_empty($validation['data'], 'id'))
            ]);
        }
        $this->response([
            'status' => false,
            'message' => setErrorDelimiter()
        ]);
    }
}